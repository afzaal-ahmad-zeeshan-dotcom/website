/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
// import { useStaticQuery, graphql } from "gatsby"

import { rhythm } from "../utils/typography"

const Tags = () => {
  // const data = useStaticQuery(graphql`
  //   query TagsQuery {
  //     markdownRemark {
  //       id
  //     }
  //   }
  // `)

  return (
    <div
      style={{
        display: `flex`,
        marginBottom: rhythm(2.5),
      }}
    >
      <p>
        Tags
      </p>
    </div>
  )
}

export default Tags
