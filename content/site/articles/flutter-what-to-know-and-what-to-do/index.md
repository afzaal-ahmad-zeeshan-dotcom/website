---
title: "Flutter Apps: What To Know and What To Do"
date: "2020-03-15T00:00:00.000Z"
description: "A rather sadistic article in which I discuss my 1+ year experience working with Flutter development and how it compares with other frameworks."
published: false
tags: [ "speak it! app", "flutter", "android studio", "android", "react native", "xamarin", "ionic", "xamarin.forms", "react", "ios development" ]
---

