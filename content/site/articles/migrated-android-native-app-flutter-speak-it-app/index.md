---
title: "Migrated an Android Studio native app to Flutter: Speak It! App Story"
date: "2020-03-15T00:00:00.000Z"
description: "A real usage of migrating an application from native Android Studio to Flutter framework."
published: false
tags: [ "speak it! app", "flutter", "android studio", "android" ]
---

