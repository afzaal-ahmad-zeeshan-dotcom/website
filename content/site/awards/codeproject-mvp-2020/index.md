---
title: "Received Award: CodeProject MVP 2020"
date: "2020-01-02T00:00:00.000Z"
description: "CodeProject community selects around 20 experts from a pool of more than 2 million experts as their MVP and I was selected for the 5th time in a row!"
author: "Afzaal Ahmad Zeeshan"
tags: [ "codeproject", "awards", "mvp", "2020" ]
published: true
type: "blog"
---

It has been 5 years that I have been a CodeProject MVP! Celebration is a must.

> CodeProject is a Canadian community for tech professionals, software engineers
> and IT pros with more than 14 million members.

I am the [top member of CodeProject](https://www.codeproject.com/Members/afzaal_ahmad_zeeshan)
from Pakistan and among top 30 overall.

![Top Pakistani member on CodeProject: Afzaal Ahmad Zeeshan](codeproject-profile-afzaal-ahmad-zeeshan-top-pakistani-member.png)

I have been an MVP at CodeProject for the past 5 years now. MVP (Most Valuable
Professional) award is given to the experts in software technology, passionate
to share their expertise with others. Don't take my word for it, here is [what
CodeProject says](https://www.codeproject.com/script/Contests/Winners.aspx?obtid=1&cmpTpId=2&cid=0):

> The Code Project's Most Valuable Professional award is given to those members
> who have contributed the most to the community in both article submissions and
> in answering questions on the site. The award is given annually.

CodeProject recently introduced two categories for their award, previously it
was just one category; MVP. Now:

1. MVA; Most valuable author, given to the top article writing experts.
1. MVE; Most valuable expert, given to the top forum and QA experts.

This restructing was introduced last year, and I have been an active MVA
and MVE for 2 years. Here is my MVA email:

![CodeProject MVA award for Afzaal Ahmad Zeeshan](codeproject-mvp-mva-2020.png)

Here is my MVE email:

![CodeProject MVE award for Afzaal Ahmad Zeeshan](codeproject-mvp-mve-2020.png)

I will update the post once my MVE and MVA certificates arrive.
